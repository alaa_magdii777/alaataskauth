import React, { Component } from 'react';
import { Text, View, ScrollView, TouchableOpacity, Image } from 'react-native';
import Font from "../utils/fonts";
import Images from "../Images";
import Icon from 'react-native-vector-icons/FontAwesome'

const NavigationDrawerStructure = (props) => {
    const toggleDrawer = () => {
        props.navigationProps.toggleDrawer();
    };

    return (
        <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity onPress={() => toggleDrawer()}>
                <Icon
                    size={25}
                    name='bars'
                    color='#4B0082' />
            </TouchableOpacity>
        </View>
    );
}

export default class Header extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let { navigation } = this.props;
        return (
            <View style={{ paddingVertical: 10, backgroundColor: "white", paddingHorizontal: 20, paddingTop: 30, elevation: 10 }}>
                <NavigationDrawerStructure navigationProps={navigation} />
            </View>
        )
    }
}
