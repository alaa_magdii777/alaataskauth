import React from 'react';
import { ListItem,Button} from 'react-native-elements';
import {View, Text,TouchableHighlight,StyleSheet,Image,ScrollView,TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import Images from "../Images";
import Font from "../utils/fonts";
import colorsTheme from "../utils/colorsTheme";

const { Family } = Font;
const { colors } = colorsTheme


export default function FeedBackCard({name,avatar,title,feedback}){
    

    const renderLeftElement=()=>  
   <View>
    <View style={{flexDirection:"row",marginBottom:30}}></View>

        <View style={styles.containerInfo}>
            <View style={styles.contactInfo}>
                <Image style={styles.avatar} source={(avatar)} />
            </View>


            <View style={styles.nameInfo}>
                <Text style={styles.titlePink}>{name}</Text>
              <View style={{flexDirection:"row"}}>

                  <View style={{flexDirection:"column",paddingHorizontal:3,}}>
                    <Text style={styles.title}>{title} </Text>
                    <Text style={styles.subtitle}>{feedback} </Text>
                   </View> 

              </View>  
            </View>
        </View>
        
        </View> ;
        
    const renderRightElement=()=>   
    <View style={styles.contactInfoRate}>
       <Button
        title="Send feed "
        titleStyle={{color:"white",fontSize:12,fontFamily:Font.Family.regular}}
        buttonStyle={{marginBottom:40,borderRadius:10,backgroundColor:colors.purpleLight,paddingHorizontal:50,height:30}}
      />

    </View>;


    return (
        <ScrollView style={styles.scrollView}>
            <ListItem 
                containerStyle={styles.container}
                rightElement={renderRightElement() } 
                leftElement={renderLeftElement()}
                bottomDivider={true}       
            />
        </ScrollView>
    )} 

    FeedBackCard.PropTypes={
    name:PropTypes.string,
    avatar:PropTypes.string.isRequired,
    title:PropTypes.string.isRequired,
    feedback:PropTypes.string.isRequired,
    
}    
const styles=StyleSheet.create({
    container:{
        flex:1,
        padding:-5,
        textAlign:'center',
        paddingBottom:5,
        paddingTop:5,
        flexDirection:'row',
    },
    containerInfo:{
        flexDirection:'row',alignItems:"center",
    },
    contactInfo:{
        paddingBottom:16,flexDirection:"row",
        alignItems:"center",
    },
    contactInfoRate:{
        paddingBottom:30,flexDirection:"row",
        alignItems:"center",marginBottom:10,marginLeft:-130
    },        
    nameInfo:{
        flexDirection:'column', padding:10,
    },
    avatar:{
        borderColor:"#707070",
        // borderWidth:1,borderRadius:50,
        width:60,padding:2,
        height:60,justifyContent:'space-between'
    },
    title:{
        color:colors.primary,flexDirection:"row",paddingVertical:5,
        fontSize:14,paddingLeft:2,fontFamily:Font.Family.semi_bold
    },
    titlePink:{
        color:'#D421B0',paddingTop:10,fontFamily:Font.Family.semi_bold,
        fontSize:12,fontWeight:"bold",paddingLeft:2,
    },
    rate:{
        color:'#858585',paddingHorizontal:5,
        fontSize:10,flexDirection:"column"
    },
    subtitle:{
        color:'black',
        paddingHorizontal:4,fontFamily:Font.Family.regular,
        fontSize:12,flexDirection:"row",
    },
    scrollView: {
        marginHorizontal:10,
      },
})


