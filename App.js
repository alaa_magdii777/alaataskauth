import React, { useEffect, useMemo, useState } from 'react';
import { SafeAreaProvider } from "react-native-safe-area-context";
import MainNavigation from "./src/navigations/MainNavigation";
import 'react-native-gesture-handler';
import AuthContext from "./src/contexts/AuthContext"; 
import RefreshAuth from "./src/contexts/RefreshAuth";

// export default class App extends Component {
const App = () => { 
  const [authData, setAuthData] = useState({});
  const [refreshAuthData, setRefreshAuthData] = useState({});
  const authContextValue = useMemo(() => ({ authData, setAuthData }), [authData]);
  const refreshAuthValue = useMemo(() => ({ refreshAuthData, setRefreshAuthData}), [refreshAuthData]);

  // render() {
  return (
    <AuthContext.Provider value={authContextValue}>
    <RefreshAuth.Provider value={refreshAuthValue}>
      <SafeAreaProvider>
        <MainNavigation />
      </SafeAreaProvider>
    </RefreshAuth.Provider>
    </AuthContext.Provider>

  )
  // }
}

export default App;




